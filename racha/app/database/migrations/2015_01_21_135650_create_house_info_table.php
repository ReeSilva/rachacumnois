<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHouseInfoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('house_info', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('description', 65535);
			$table->integer('rooms');
			$table->boolean('movables')->default(0);
			$table->boolean('garage')->default(0);
			$table->boolean('internet')->default(0);
			$table->boolean('cable_tv')->default(0);
			$table->boolean('air')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('house_info');
	}

}
