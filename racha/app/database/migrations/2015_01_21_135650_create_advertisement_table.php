<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdvertisementTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('advertisement', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('house_id')->index('fk_advertisement_house1_idx');
			$table->integer('users_id')->index('fk_advertisement_users1_idx');
			$table->text('description', 65535);
			$table->decimal('price');
			$table->dateTime('time')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('advertisement');
	}

}
