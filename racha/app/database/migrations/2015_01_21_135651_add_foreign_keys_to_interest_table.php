<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToInterestTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('interest', function(Blueprint $table)
		{
			$table->foreign('advertisement_id', 'fk_interest_advertisement1')->references('id')->on('advertisement')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('users_id', 'fk_interest_users1')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('interest', function(Blueprint $table)
		{
			$table->dropForeign('fk_interest_advertisement1');
			$table->dropForeign('fk_interest_users1');
		});
	}

}
