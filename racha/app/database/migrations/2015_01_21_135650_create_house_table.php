<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHouseTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('house', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('house_info_id')->index('fk_house_house_info1_idx');
			$table->integer('house_type_id')->index('fk_house_house_type1_idx');
			$table->integer('zipcode');
			$table->string('address', 100);
			$table->integer('number');
			$table->string('neighborhood', 45)->nullable();
			$table->string('city', 45);
			$table->string('state', 45);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('house');
	}

}
