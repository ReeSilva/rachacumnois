<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToAdvertisementTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('advertisement', function(Blueprint $table)
		{
			$table->foreign('house_id', 'fk_advertisement_house1')->references('id')->on('house')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('users_id', 'fk_advertisement_users1')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('advertisement', function(Blueprint $table)
		{
			$table->dropForeign('fk_advertisement_house1');
			$table->dropForeign('fk_advertisement_users1');
		});
	}

}
