<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToHouseTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('house', function(Blueprint $table)
		{
			$table->foreign('house_info_id', 'fk_house_house_info1')->references('id')->on('house_info')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('house_type_id', 'fk_house_house_type1')->references('id')->on('house_type')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('house', function(Blueprint $table)
		{
			$table->dropForeign('fk_house_house_info1');
			$table->dropForeign('fk_house_house_type1');
		});
	}

}
