<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInterestTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('interest', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('users_id')->index('fk_interest_users1_idx');
			$table->integer('advertisement_id')->index('fk_interest_advertisement1_idx');
			$table->text('why', 65535);
			$table->dateTime('visit_date')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('interest');
	}

}
