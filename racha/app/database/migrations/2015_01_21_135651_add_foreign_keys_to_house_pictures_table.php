<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToHousePicturesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('house_pictures', function(Blueprint $table)
		{
			$table->foreign('house_id', 'fk_house_pictures_house1')->references('id')->on('house')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('house_pictures', function(Blueprint $table)
		{
			$table->dropForeign('fk_house_pictures_house1');
		});
	}

}
