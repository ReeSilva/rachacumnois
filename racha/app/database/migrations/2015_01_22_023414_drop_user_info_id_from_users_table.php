<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropUserInfoIdFromUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table("users", function(Blueprint $table)
		{
			$table->dropColumn('user_info_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table("users", function(Blueprint $table)
		{
			$table->integer('user_info_id')->index('fk_users_user_info1_idx');
		});
	}

}
