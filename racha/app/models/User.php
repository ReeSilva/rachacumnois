<?php

namespace RachaApp\Models;

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

/**
 * This model will handle all the actions to users table and users
 * business rules.
 * @package RachaApp\Models
 */
class User extends \Eloquent implements UserInterface, RemindableInterface
{

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array(
		'password',
		'remember_token'
	);

	/**
	 * @var array
     */
	protected $fillable = array(
		'first_name',
		'last_name',
		'email',
		'password',
		'created_at',
		'updated_at'
	);

	/**
	 * This action will receive an array with the user
	 * data and return a false if another user with that
	 * email exists or the object created for the new user.
	 *
	 * @param array $data
	 * @return bool|static
     */
	public function storeUser(array $data)
	{
		$return = array();

		if($this->where("email", "=", $data['email'])->count() > 0)
		{
			$return = array();

			$return['status'] = 409;
			$return['message'] = "Este usuário já existe.";

			return $return;
		}

		$return['status'] = 201;
		$return['message'] = "Usuário criado com sucesso.";
		$return['user'] = $this->create($data);

		return $return;
	}

}
