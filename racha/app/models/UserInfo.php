<?php

class UserInfo extends \Eloquent {
	protected $fillable = [
		'description',
		'cpf',
		'doc_id',
		'job',
		'created_at'
	];
}