<?php

namespace RachaApp\Controllers;


use Illuminate\Http\Response;
use RachaApp\Models\User;
use Input as Input;
use Hash as Hash;

class UsersController extends BaseController {

	/**
	 * Receive the login information of user and return
	 * in success or no.
	 * POST /user/login
	 *
	 * @return Response
	 */
	public function login()
	{
		$return = array();

		$userData = array(
			'email'    => Input::get("email"),
			'password' => Input::get("password"),
		);

		if(\Auth::attempt($userData, true))
		{
			$user = \Auth::user();

			$return['status']  = 200;
			$return['message'] = "Usuário logado com sucesso";
			$return['user']    = $user;

			return \Response::json($return, 200);
		}

		$return['status']  = 401;
		$return['message'] = "E-mail ou senha incorretos";

		return \Response::json($return, 401);
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /user/create
	 *
	 * @return Response
	 */
	public function store()
	{
		$user    = new User();
		$newUser = array();

		$newUuser['first_name'] = Input::get("first_name");
		$newUser['last_name']   = Input::get("last_name");
		$newUser['email']       = Input::get("email");
		$newUser['password']    = Hash::make(\Input::get("password"));
		$newUser['created_at']  = date("Y-m-d H:i:s");

		$response = $user->storeUser($newUser);

		return \Response::json($response, $response['status']);
	}

	/**
	 * Display the specified resource.
	 * GET /user/{id}/info
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function storeInfo($id)
	{
		return \Response::make("Usuário editado com sucesso.", 200);
	}

	/**
	 * Show the form for editing the specified resource.
	 * PUT /user/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = User::find($id);

		foreach(Input::all() as $key => $value)
		{
			if($key == "password")
			{
				$user->$key = Hash::make($value);
			}

			$user->$key = $value;
		}

		if ($user->save())
		{
			return \Response::make("Usuário editado com sucesso", 200);
		}

		return \Response::make("Ocorreu um erro ao editar o usuário.", 403);
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /user/{id}/delete
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}