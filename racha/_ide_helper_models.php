<?php
/**
 * An helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace RachaApp\Models{
/**
 * This model will handle all the actions to users table and users
 * business rules.
 *
 * @package RachaApp\Models
 * @property integer $id 
 * @property string $first_name 
 * @property string $last_name 
 * @property string $email 
 * @property string $password 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property string $remember_token 
 * @property integer $user_info_id 
 * @method static \Illuminate\Database\Query\Builder|\RachaApp\Models\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\RachaApp\Models\User whereFirstName($value)
 * @method static \Illuminate\Database\Query\Builder|\RachaApp\Models\User whereLastName($value)
 * @method static \Illuminate\Database\Query\Builder|\RachaApp\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\RachaApp\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\RachaApp\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\RachaApp\Models\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\RachaApp\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\RachaApp\Models\User whereUserInfoId($value)
 */
	class User {}
}

